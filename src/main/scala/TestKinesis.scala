import java.nio.ByteBuffer

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.kinesis.{AmazonKinesisAsync, AmazonKinesisAsyncClientBuilder}

import scala.util.Try

object TestKinesis {

  def main(args: Array[String]): Unit = {

    import scala.collection.JavaConversions._
    import com.amazonaws.services.kinesis.AmazonKinesisClientBuilder

    val endpoint = new EndpointConfiguration("http://127.0.0.1:4567", "us-west-1")

    val kinesis: AmazonKinesisAsync = AmazonKinesisAsyncClientBuilder.standard.
      withEndpointConfiguration(endpoint).build()

    val streamName = "testjustin1"

    val createStream = Try(kinesis.createStream(streamName, 1))

    Thread.sleep(4000)

    val testData = new Array[Byte](10)

    val writeStream = kinesis.putRecord(streamName, ByteBuffer.wrap(testData), "1")

    val listStreams = kinesis.listStreams().getStreamNames()

    listStreams.foreach{
      println(_)
    }

  }



}
