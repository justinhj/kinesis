name := "kinesis"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  "com.amazonaws" % "aws-java-sdk" % "1.11.250"
)

